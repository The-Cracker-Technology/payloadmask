rm -rf /opt/ANDRAX/payloadmask
mkdir /opt/ANDRAX/payloadmask

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip payloadmask

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf payloadmask /opt/ANDRAX/payloadmask
cp -Rf payloads /opt/ANDRAX/payloadmask

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
